
<html>
    <!--
        webuse_tempws_api.php
        php program that uses the tempws_api.php web service to convert
        temperatures from F to C or C to F
    -->
    <head>
    </head>

    <body>
        <h1>webuse_tempws_api.php</h1>
        <h2>Consume (use) the tempws_api.php Web Service <br />
            which converts temperatures between Celsius and Fahrenheit
        </h2>
        <form method="post" action="tempws_api.php">
            <p>
                Enter a temperature and scale below: <br />
                <input type="text" name="temp" size="4" maxlength="4" />
                Select Temp Scale:
                <input type="radio" name="scale" value="F" checked="checked" />F
                <input type="radio" name="scale" value="C" />C
            </p>
            <p>
                Converted temperature is:  <br />
                <input type="text" name="outtemp" size="8" maxlength="8"
                       onfocus="blur()"
                       <?php echo "value = \"$output\""; ?> />

            </p>
            <p>
                <input type="submit" value="Convert Temperature" />
            </p>
        </form>
    </body>
</html>