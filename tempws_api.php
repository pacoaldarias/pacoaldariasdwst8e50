<?php
/* tempws_api.php
  accepts get parameter of temp = [+/-] float
  scale = F or C
  (Fahrenheit or Celsius temperature), returns the converted temp as +/-(int)F

  if no parameters, then returns source code and description of interface to web service

 */



if (isset($_GET['temp'])) {
    $temp = floatval($_GET['temp']);
    $scale = $_GET['scale'];
    $strtemp = "error";
    $newtemp = 0.0;

    if ($scale == 'F') {
        $newtemp = (($temp - 32) * 5) / 9;
        $strtemp = number_format($newtemp, 2);
        $strtemp = $strtemp . "°" . ' C';
    }

    if ($scale == 'C') {
        $newtemp = (($temp * 9) / 5) + 32;
        $strtemp = number_format($newtemp);
        $strtemp = $strtemp . "°" . ' F';
    }

    echo "$strtemp";
} # end of section where $temp has a value
else {  // no temp specified in call to the web service
    ?>  <!-- close php so we can output an html page -->
    <html>
        <head><title>Temp Web Service</title>
        </head>
        <body>
            <h2>Temperature Converter Web Service</h2>
            <div style="font-weight:bold;border-style:solid;border-width:5px;padding:10px;">
                <p>
                    Just provide this service with a URL that includes a GET parameter like the following.<br />
                <blockquote style="font-weight:bold">?temp=40&scale=F</blockquote>
                The temp parameter is just a number that represents the temperature in either Fahrenheit or Celsius.
                If provided a Celsius
                temperature, the web service will
                return a string, like 52.2F.  If provided a Fahrenheit temperature, it will return a Celsius...
                Here's the URL:<br /><span style="color:red">
                    http://webdev01.radford.edu:13280/ws/tempws_api.php</span><br />
                Here's the URL with an appended GET parameter: <br /><span style="color:red">
                    http://webdev01.radford.edu:13280/ws/tempws_api.php?temp=40&scale=F</span>
            </p>
        </div>
    </body>
    </html>
    <?php
    # restart the php
}
?>